# Axioma Test Tasks
## Work Log

* Layout - 10 hours
* Implement environment (AngularJS skeleton & Mini framework PHP) - 8 hours
* Implement REST API - 8 hours
* Implement FrontEnd - 8 hours

## Struct
```
/axioma
  /api - BackEnd PHP Code
  /frontend - FrontEnd Code (AngularJS)
```

* @TODO Implement `production /development` environment splitting
* @TODO Create config for JS project
* @TODO Implement Cache provider
* @TODO Implement Logger provider
* @TODO Responsive Layout for test tasks

## FrontEnd Install Instructions

1. Install Node.js
2. Install grunt-cli globally npm install -g grunt-cli
3. Install bower.io
4. Clone project
5. Execute `cd ./frontend`
6. Execute `npm install`
7. Execute `bower install`
8. Execute `grunt`
9. Execute `npm start`

## BackEnd Install Instructions

1. Prepare test server with nginx
2. Clone /api folder to server
3. Config nginx.conf file
4. Put it to server nginx
5. Generate SSL sertificate
6. If you change domain please change `/frontend/app/js/services.js` to correct URL (string `var _url=""`)