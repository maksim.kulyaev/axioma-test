var staff = angular.module('staff', []);

angular.module('staff').component('staff', {
    templateUrl: 'staff-list.html',
    controller: function StaffCtrl($http, Resource) {
        var self = this;

        var init = function () {
            self.message = {
                show: false,
                text: '',
                class: '',
                callback: null
            }

            self.sorting = {
                order: 'id',
                direction: 'asc'
            }

            self.services.load();
        };

        self.services = {
            load: function () {
                Resource.positions().load (
                    {},
                    function (response) {
                        self.positions = response.data;
                        console.log(self.positions);
                        Resource.staff().load(
                            self.sorting,
                            function (response) {
                                self.staff = response.data;
                            },
                            function (response) {
                                self.services.showError(
                                    'Loading staff failed!',
                                    function () {
                                        console.log(response);
                                    }
                                );
                            }
                        );
                    },
                    function (response) {
                        self.services.showError(
                            'Loading positions failed!',
                            function () {
                                console.log(response);
                            }
                        );
                    }
                )
            },
            showError: function (text, callback) {
                self.message = {
                    show: true,
                    text: text,
                    class: 'alert-danger',
                    callback: callback
                };
            }
        };

        self.events = {
            sort: function (field) {
                if (field == self.sorting.order) {
                    switch (self.sorting.direction) {
                        case 'asc':
                            return 'glyphicon-arrow-up';
                        case 'desc':
                            return 'glyphicon-arrow-down';
                        default:
                            return '';
                    }
                } else {
                    return '';
                }
            }
        };

        self.actions = {
            remove: function (id) {
                if (window.confirm('Are you sure?')) {
                    Resource.staff().delete(
                        id,
                        function (response) {
                            self.services.load();
                        },
                        function (response) {
                            self.services.showError(
                                'Deleting failed!',
                                function () {
                                    console.log(response);
                                }
                            );
                        }
                    );
                }
            },
            sort: function (field) {
                self.sorting.order = field;
                if (self.sorting.order == field) {
                    if ('asc' == self.sorting.direction) {
                        self.sorting.direction = 'desc';
                    } else {
                        self.sorting.direction = 'asc';
                    }
                } else {
                    self.sorting.direction = 'asc';
                }
                self.services.load();
            },
            viewPosition: function (id) {
                for (position in self.positions) {
                    if (self.positions[position].id == id) {
                        return self.positions[position].id + '. ' + self.positions[position].title;
                    }
                }

                return '';
            }
        };

        init();
    }
}).component('staffDetail', {
    templateUrl: 'staff.html',
    controller: function StaffDetailCtrl($scope, $location, $routeParams, $http, Resource) {
        var self = this;
        console.log($routeParams.id);
        self.id = $routeParams.id;

        var init = function () {
            self.regex = '\\d+';

            self.message = {
                show: false,
                text: '',
                class: '',
                callback: null
            }

            if ('new' == self.id) {
                self.id = false;
            } else {
                self.position = {
                    position_id: '',
                    fname: '',
                    lname: '',
                    revenue: '',
                    description: ''
                }
            }

            self.validation = {
                fname: {
                    error: false,
                    success: false
                },
                lname: {
                    error: false,
                    success: false
                },
                revenue: {
                    error: false,
                    success: false
                },
                position: {
                    error: false,
                    success: false
                },
                description: {
                    error: false,
                    success: false
                }
            }

            self.services.view(self.id);
        };

        self.services = {
            view: function (id) {
                Resource.positions().load(
                    {},
                    function (response) {
                        self.positions = response.data;
                        if (self.id) {
                            Resource.staff().view(
                                self.id,
                                function (response) {
                                    self.staff = response.data;
                                    console.log(self.staff);
                                },
                                function (response) {
                                    self.services.showError(
                                        'View Staff failed!',
                                        function () {
                                            console.log(response);
                                        }
                                    );
                                }
                            );
                        }
                    },
                    function (response) {
                        self.services.showError(
                            'Loading positions failed!',
                            function () {
                                console.log(response);
                            }
                        );
                    }
                );
            },
            validate: function () {
                var form = $scope.aForm;

                var validated = true;

                if (form.staffFname.$error.required) {
                    validated = false;
                    self.validation.fname.error = true;
                }

                if (form.staffFname.$error.maxlength) {
                    validated = false;
                    self.validation.fname.error = true;
                }

                if (form.staffFname.$error.minlength) {
                    validated = false;
                    self.validation.fname.error = true;
                }

                if (validated) {
                    self.validation.fname.success = true;
                    self.validation.fname.error = false;
                }

                var lnValidated = true;
                if (form.staffLname.$error.required) {
                    validated = false;
                    lnValidated = false;
                    self.validation.lname.error = true;
                }

                if (form.staffLname.$error.maxlength) {
                    validated = false;
                    lnValidated = false;
                    self.validation.lname.error = true;
                }

                if (form.staffLname.$error.minlength) {
                    validated = false;
                    lnValidated = false;
                    self.validation.lname.error = true;
                }

                if (lnValidated) {
                    self.validation.lname.success = true;
                    self.validation.lname.error = false;
                }

                var pValidated = true;
                if (form.staffPosition.$error.required) {
                    validated = false;
                    pValidated = false;
                    self.validation.position.error = true;
                }

                if (pValidated) {
                    self.validation.position.success = true;
                    self.validation.position.error = false;
                }

                var rValidated = true;
                if (!form.staffRevenue.$valid) {
                    validated = false;
                    rValidated = false;
                    self.validation.revenue.error = true;
                }

                if (form.staffRevenue.$error.maxlength) {
                    validated = false;
                    rValidated = false;
                    self.validation.revenue.error = true;
                }

                if (form.staffRevenue.$error.minlength) {
                    validated = false;
                    rValidated = false;
                    self.validation.revenue.error = true;
                }

                if (rValidated) {
                    self.validation.revenue.success = true;
                    self.validation.revenue.error = false;
                }

                var dValidated = true;
                if (form.staffDescription.$error.maxlength) {
                    validated = false;
                    dValidated = false;
                    self.validation.description.error = true;
                }

                if (form.staffDescription.$error.minlength) {
                    validated = false;
                    dValidated = false;
                    self.validation.description.error = true;
                }

                if (dValidated) {
                    self.validation.description.success = true;
                    self.validation.description.error = false;
                }

                return validated;

            },
            showMessage: function (text, callback) {
                self.message = {
                    show: true,
                    text: text,
                    class: 'alert-success',
                    callback: callback
                };
            },
            showError: function (text, callback) {
                self.message = {
                    show: true,
                    text: text,
                    class: 'alert-danger',
                    callback: callback
                };
            }
        };

        self.events = {
            onchange: function () {
                console.log('Changed!');
            }
        }

        self.actions = {
            save: function () {
                if (!self.services.validate()) {
                    return;
                }

                Resource.staff().save(
                    self.staff,
                    function (response) {
                        if (self.staff.id) {
                            self.services.showMessage(
                                'Saved!',
                                function () {
                                    self.services.view(self.staff.id);
                                }
                            );
                        } else {
                            self.services.showMessage(
                                'Saved!',
                                function () {
                                    $location.path('/staff/' + response.data.id);
                                }
                            );
                        }
                    },
                    function (response) {
                        self.services.showError(
                            'Saving failed!',
                            function () {
                                console.log(response);
                            }
                        );
                    }
                )
            },
            back: function () {
                $location.path('/staff');
            }
        };

        init();
    }
});
