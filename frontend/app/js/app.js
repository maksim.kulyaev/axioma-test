// Define the `phonecatApp` module
var axioma = angular.module('axioma', ['ngRoute', 'positions', 'staff']);
angular.module('axioma').config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
        $locationProvider.html5Mode(true);

        $routeProvider.when('/positions', {
            template: '<positions></positions>'
        }).when('/positions/:id', {
            template: '<position-detail></position-detail>'
        }).when('/staff', {
            template: '<staff></staff>'
        }).when('/staff/:id', {
            template: '<staff-detail></staff-detail>'
        }).otherwise('/positions');
    }
]);