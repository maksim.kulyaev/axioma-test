var positions = angular.module('positions', ['services']);

angular.module('positions').component('positions', {
    templateUrl: 'positions-list.html',
    controller: function PositionsCtrl($http, Resource) {
        var self = this;

        var init = function () {
            self.message = {
                show: false,
                text: '',
                class: '',
                callback: null
            }

            self.sorting = {
                order: 'id',
                direction: 'asc'
            }

            self.services.load();
        };

        self.services = {
            load: function () {
                Resource.positions().load(
                    self.sorting,
                    function (response) {
                        self.positions = response.data;
                    },
                    function (response) {
                        self.services.showError(
                            'Loading positions failed!',
                            function () {
                                console.log(response);
                            }
                        );
                    }
                );
            },
            showError: function (text, callback) {
                self.message = {
                    show: true,
                    text: text,
                    class: 'alert-danger',
                    callback: callback
                };
            }
        };

        self.events = {
            sort: function (field) {
                if (field == self.sorting.order) {
                    switch (self.sorting.direction) {
                        case 'asc':
                            return 'glyphicon-arrow-up';
                        case 'desc':
                            return 'glyphicon-arrow-down';
                        default:
                            return '';
                    }
                } else {
                    return '';
                }
            }
        };

        self.actions = {
            remove: function (id) {
                if (window.confirm('Are you sure?')) {
                    Resource.positions().delete(
                        id,
                        function (response) {
                            self.services.load();
                        },
                        function (response) {
                            self.services.showError(
                                'Deleting failed!',
                                function () {
                                    console.log(response);
                                }
                            );
                        }
                    );
                }
            },
            sort: function (field) {
                self.sorting.order = field;
                if (self.sorting.order == field) {
                    if ('asc' == self.sorting.direction) {
                        self.sorting.direction = 'desc';
                    } else {
                        self.sorting.direction = 'asc';
                    }
                } else {
                    self.sorting.direction = 'asc';
                }
                self.services.load();
            }
        };

        init();
    }
}).component('positionDetail', {
    templateUrl: 'position.html',
    controller: function PositionDetailCtrl($location, $routeParams, $http, Resource) {
        var self = this;
        console.log($routeParams.id);
        self.id = $routeParams.id;

        var init = function () {
            self.message = {
                show: false,
                text: '',
                class: '',
                callback: null
            }

            if ('new' !== self.id) {
                self.services.view(self.id);
            } else {
                self.position = {
                    title: '',
                    description: ''
                }
            }
        };

        self.services = {
            view: function (id) {
                Resource.positions().view(
                    id,
                    function (response) {
                        self.position = response.data;
                        console.log(self.position);
                    },
                    function (response) {
                        self.errors.common = {
                            code: response.status,
                            message: 'Deleting failed'
                        }
                    }
                );
            },
            showMessage: function (text, callback) {
                self.message = {
                    show: true,
                    text: text,
                    class: 'alert-success',
                    callback: callback
                };
            },
            showError: function (text, callback) {
                self.message = {
                    show: true,
                    text: text,
                    class: 'alert-danger',
                    callback: callback
                };
            }
        };

        self.events = {
            onchange: function () {
                console.log('Changed!');
            }
        }

        self.actions = {
            save: function () {
                if (!self.position.title) {
                    return false;
                }

                if (self.position.title.length < 3) {
                    return false;
                }

                Resource.positions().save(
                    self.position,
                    function (response) {
                        if (self.position.id) {
                            self.services.showMessage(
                                'Saved!',
                                function () {
                                    self.services.view(self.position.id);
                                }
                            );
                        } else {
                            self.services.showMessage(
                                'Saved!',
                                function () {
                                    $location.path('/positions/' + response.data.id);
                                }
                            );
                        }
                    },
                    function (response) {
                        self.services.showError(
                            'Saving failed!',
                            function () {
                                console.log(response);
                            }
                        );
                    }
                )
            },
            back: function () {
                $location.path('/positions');
            }
        };

        init();
    }
});
