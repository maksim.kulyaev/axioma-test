var services = angular.module('services', []);

angular.module('services')
    .factory(
        'Resource', function ($http) {
            var url = 'https://api.axioma.test/';

            var serialize = function (params) {
                var query = [];
                for (var key in params) {
                    if (params.hasOwnProperty(key)) {
                        query.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
                    }
                }

                return query.join('&');
            }

            var Resource = function (resource) {
                return {
                    load: function (params, success, error) {
                        console.log(params);
                        console.log(serialize(params));
                        $http.get(url + resource + '/?' + serialize(params)).then(
                            function (response) {
                                console.log(response);
                                success(response)
                            },
                            function (response) {
                                console.log(response);
                                error(response);
                            }
                        )
                    },
                    view: function (id, success, error) {
                        $http.get(url + resource + '/' + id).then(
                            function (response) {
                                console.log(response);
                                success(response)
                            },
                            function (response) {
                                console.log(response);
                                error(response);
                            }
                        )
                    },
                    delete: function (id, success, error) {
                        $http.delete(url + resource + '/' + id).then(
                            function (response) {
                                console.log(response);
                                success(response)
                            },
                            function (response) {
                                console.log(response);
                                error(response);
                            }
                        )
                    },
                    save: function (data, success, error) {
                        var config = {
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        }
                        if (data.id) {
                            $http.put(url + resource + '/' + data.id, data, config).then(
                                function (response) {
                                    console.log(response);
                                    success(response)
                                },
                                function (response) {
                                    console.log(response);
                                    error(response);
                                }
                            )
                        } else {
                            $http.post(url + resource, data, config).then(
                                function (response) {
                                    console.log(response);
                                    success(response)
                                },
                                function (response) {
                                    console.log(response);
                                    error(response);
                                }
                            )
                        }
                    }
                }
            }

            var positions = function () {
                return Resource('positions');
            }

            var staff = function () {
                return Resource('staff');
            };

            return {
                positions: positions,
                staff: staff
            }
        }
    ).directive(
        'formMessage', ['$timeout', function ($timeout) {
            return {
                restrict: 'E',
                scope: {
                    message: '='
                },
                templateUrl: 'message.html',
                link: function (scope, element, attr) {
                    console.log('Message Linked!')

                    var reset = function () {
                        scope.message = {
                            show: false,
                            text: '',
                            class: '',
                            callback: false
                        }
                    };

                    scope.$watch('message', function (current, prev) {
                        if (current.show) {
                            $timeout(function () {
                                reset();
                                current.callback && current.callback();
                                console.log('Callback called!');
                            }, 2000)
                        }
                    });
                }
            }
        }]
    );
