module.exports = function (grunt) {
    require('grunt-task-loader')(grunt);

    // Project configuration.
    grunt.initConfig({
        config: grunt.file.readJSON('package.json'),
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: '<%= config.options.tmp %>',
                    src: ['*.css', '!*.min.css'],
                    dest: '<%= config.options.build %>/css',
                    ext: '.min.css'
                }]
            }
        },
        jade: {
            html: {
                files: {
                    '<%= config.options.build %>': ['<%= config.options.src %>/templates/*.jade']
                },
                options: {
                    pretty: true,
                    client: false
                }
            }
        },
        clean: {
            tmp: '<%= config.options.tmp %>',
            build: '<%= config.options.build %>'
        },
        stylus: {
            options: {
                compress: false
            },
            compile: {
                files: {
                    '<%= config.options.tmp %>/main.css': ['<%= config.options.src %>/css/*.styl'] // compile and concat into single file
                }
            }
        },
        copy: {
            fonts: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: [
                            '<%= config.options.src %>/fonts/*.ttf',
                            '<%= config.options.src %>/fonts/*.woff',
                            '<%= config.options.src %>/fonts/*.eot',
                            '<%= config.options.src %>/fonts/*.svg',
                            './bower_components/bootstrap/dist/fonts/*.ttf',
                            './bower_components/bootstrap/dist/fonts/*.woff',
                            './bower_components/bootstrap/dist/fonts/*.woff2'
                        ],
                        dest: '<%= config.options.build %>/fonts/'
                    }
                ]
            },
            images: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: '<%= config.options.src %>/images/*.png',
                        dest: '<%= config.options.build %>/img/'
                    }
                ]
            },
            css: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: [
                            './bower_components/bootstrap/dist/css/*.css',
                            '!./bower_components/bootstrap/dist/css/*.min.css'
                        ],
                        dest: '<%= config.options.tmp %>/'
                    }
                ]
            },
            js: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: [
                            './bower_components/angular/angular.js',
                            './bower_components/angular-route/angular-route.js',
                            './bower_components/jquery/dist/jquery.js',
                            './bower_components/bootstrap/dist/js/bootstrap.js'
                        ],
                        dest: '<%= config.options.tmp %>/',
                        ext: '.min.js'
                    }
                ]
            },
            src: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        src: [
                            './bower_components/angular/angular.js',
                            './bower_components/angular-route/angular-route.js',
                            './bower_components/jquery/dist/jquery.js',
                            './bower_components/bootstrap/dist/js/bootstrap.js'
                        ],
                        dest: '<%= config.options.build %>/js/',
                        ext: '.min.js'
                    },
                    {
                        expand: true,
                        flatten: true,
                        cwd: '<%= config.options.src %>/js/',
                        src: '**/*.js',
                        dest: '<%= config.options.build %>/js/app/'
                    }
                ]
            }
        },
        uglify: {
            scripts: {
                options: {
                    mangle: false
                },
                files: [
                    {
                        expand: true,
                        cwd:'<%= config.options.tmp %>/',
                        src: '**/*.js',
                        dest: '<%= config.options.build %>/js',
                    }
                ]
            },
            source: {
                options: {
                    mangle: false,
                },
                files: [{
                    expand: true,
                    cwd: '<%= config.options.src %>/js/',
                    src: '**/*.js',
                    dest: '<%= config.options.build %>/js/app'
                }]
                /*
                files: {
                    '<%= config.options.build %>/js/app.js': [
                        '<%= config.options.src %>/js/app.js',
                        '<%= config.options.src %>/js/positions.js',
                        '<%= config.options.src %>/js/staff.js',
                        '<%= config.options.src %>/js/dialog.js'
                    ]
                }
                */
            }
        },
        watch: {
            scripts: {
                files: ['Gruntfile.js', '<%= config.options.src %>/*'],
                tasks: ['build'],
                options: {
                    interrupt: true,
                    reload: true
                },
            },
        },
    });

    // Default task(s).
    grunt.registerTask(
        'build',
        [
            //'clean', 'copy:fonts', 'copy:images', 'stylus', 'cssmin', 'jade', 'clean:tmp'
            //'clean', 'copy:css', 'copy:js', 'copy:fonts', 'copy:images', 'cssmin', 'uglify:scripts', 'uglify:source', 'jade', 'clean:tmp'
            'clean', 'copy:css', 'copy:js', 'copy:fonts', 'copy:images', 'stylus', 'cssmin', 'copy:src', 'jade', 'clean:tmp'
        ]
    );

    grunt.registerTask(
        'watch',
        [
            'build', 'watch'
        ]
    );

    grunt.registerTask(
        'default',
        [
            'build'
        ]
    );

    // соната

};