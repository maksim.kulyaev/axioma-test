<?php

/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 20:00
 */

// 14.09 - 6 hrs
// 15.09 - 8 hrs
// 16.09 - 8 hrs
// 17.09 - 5 hrs
// 18.09 - 8 hrs
//phpinfo();
error_reporting(E_ALL);

set_error_handler(function ($errno, $errstr, $errfile, $errline) {
    /**
     * Convert all errors to \Exception
     */
    throw new \Exception("$errno: $errstr in $errfile on line $errline", 500);
});

set_exception_handler(function (\Exception $e) {
    /**
     * Register Exception Handler for Application
     */
    $handler = new \App\ExceptionHandler($e);
    $handler->handle();
});

require_once __DIR__ . '/../bootstrap/autoload.php';
$config = require_once __DIR__ . '/../config/config.php';

$app = new \App\Application($config);
$app->execute();