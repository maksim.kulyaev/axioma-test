<?php

return [
    'request' => \App\Core\Request::class,
    'router' => \App\Core\Router::class,
    'resources' => [
        'positions' => \App\Controllers\Positions::class,
        'staff' => \App\Controllers\Staff::class,
    ],
    'routes' => [
        // root directory
        '@' => [

        ],
        'single/route/test/:id' => [
            'GET' => [
                'controller' => \App\Controllers\Positions::class,
                'action' => 'test',
                'params' => [
                    'id' => 'integer',
                    'strict' => true,
                ]
            ]
        ]
    ],
    'response' => \App\Core\Response::class,
    'db' => \App\Providers\DbProvider::class,
    'db.options' => [
        'mysql' => [
            'host' => 'localhost',
            'username' => 'root',
            'password' => 'Axioma',
            'database' => 'axioma',
        ],
    ]
    //'cache' => \App\Providers\CacheProvider::class,
    //'logger' => \App\Providers\LoggerProvider::class,
];