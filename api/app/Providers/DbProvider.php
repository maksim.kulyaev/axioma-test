<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 20:07
 */

namespace App\Providers;


class DbProvider
{
    private $connect;

    public function __construct($options)
    {
        if (isset($options['mysql'])) {
            $connection = $options['mysql'];
            $this->connect = new \mysqli($connection['host'], $connection['username'], $connection['password'], $connection['database']);
            if ($this->connect->connect_errno) {
                throw new \Exception("Failed to connect to MySQL: " . $this->connect->connect_error, 500);
            }
        } else {
            throw new \Exception('Unsupported Database Provider', 500);
        }
    }

    private function bind($query, $params)
    {
        foreach ($params as $param) {
            switch ($param['type']) {
                case 'i':
                    $value = (int)$param['value'];
                    break;
                case 'd':
                    $value = (float)$param['value'];
                    break;
                case 's':
                default:
                    $value = "'" . $this->connect->real_escape_string($param['value']) . "'";
                    break;
            }
            $query = preg_replace('/\?/', $value, $query, 1);
        }

        return $query;
    }

    public function select($query, $params)
    {
        $data = [];
        $query = $this->bind($query, $params);
        $result = $this->connect->query($query);
        if (!$result) {
            throw new \Exception($this->connect->error, $this->connect->errno);
        }
        while ($row = $result->fetch_assoc()) {
            $data[] = $row;
        }

        return $data;
    }

    public function insert($query, $params)
    {
        $query = $this->bind($query, $params);
        $result = $this->connect->query($query);
        if (!$result) {
            throw new \Exception($this->connect->error, $this->connect->errno);
        }
        return $this->connect->insert_id;
    }

    public function update($query, $params)
    {
        $query = $this->bind($query, $params);
        $result = $this->connect->query($query);
        if (!$result) {
            throw new \Exception($this->connect->error, $this->connect->errno);
        }
        return $this->connect->affected_rows;
    }

    public function alter($query)
    {
        $result = $this->connect->query($query);
        if (!$result) {
            throw new \Exception($this->connect->error, $this->connect->errno);
        }
    }
}