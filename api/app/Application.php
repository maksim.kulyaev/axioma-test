<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 20:03
 */

namespace App;

use App\Core\Request;
use App\Core\Router;
use App\Providers\DbProvider;

class Application
{
    /**
     * @var array
     */
    private $config;
    
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Router
     */
    private $router;

    /**
     * Application constructor.
     * @param array $config
     *
     */
    public function __construct($config)
    {
        $this->config = $config;

        if (false !== strpos($_SERVER['REQUEST_URI'], '?')) {
            list($path, $query) = explode('?', $_SERVER['REQUEST_URI']);
            $path = trim($path, '/');
        } else {
            $path = trim($_SERVER['REQUEST_URI'], '/');
            $query = false;
        }
        $this->config['method'] = $_SERVER['REQUEST_METHOD'];
        $this->config['path'] = $path ? $path : '@';
        $this->config['query'] = $query;

        $this->request = new $this->config['request']();
        $this->router = new $this->config['router']($this->request, $this->get('resources'), $this->get('routes'));
        $this->db = new $this->config['db']($this->get('db.options'));
    }

    /**
     * @param string $name
     * @return mixed|null
     */
    public function get($name)
    {
        return isset($this->config[$name]) ? $this->config[$name] : null;
    }

    /**
     * @return Request
     */
    public function request()
    {
        return $this->request;
    }

    /**
     * @return Router
     */
    public function router()
    {
        return $this->router;
    }

    /**
     * @return DbProvider
     */
    public function db()
    {
        return $this->db;
    }

    private function route()
    {
        return $this->router->dispatch($this->get('method'), $this->get('path'));
    }

    public function execute()
    {
        $this->route();
        $controller = $this->router->controller;
        $action = $this->router->action;
        $request = $this->request;

        $controller = new $controller($this);
        if (is_callable([$controller, $action])) {
            $response = $controller->$action($request);
        } else {
            throw new \Exception('Action does not exist in controller', 404);
        }

        $response->json();
    }
}