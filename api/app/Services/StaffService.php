<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 20:07
 */

namespace App\Services;


use App\Application;

class StaffService
{
    protected $app;
    protected $db;
    protected $schema = '
        CREATE TABLE staff (
                     id INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
                     position_id INT DEFAULT NULL,
                     fname CHAR (255) DEFAULT NULL,
                     lname CHAR (255) DEFAULT NULL,
                     revenue DECIMAL DEFAULT 0,
                     description TINYTEXT,
                     INDEX idx_position_id (position_id),
                     FOREIGN KEY (position_id)
                        REFERENCES `position`(id)
                        ON DELETE SET NULL
                        ON UPDATE CASCADE                        
        ) ENGINE=InnoDB;
    ';

    public function instantiate() {
        $drop = 'DROP TABLE IF EXISTS staff';
        $this->db->alter($drop);
        $this->db->alter($this->schema);
    }

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->db = $app->db();
    }

    public function get($params, $orders) {
        $sql = "SELECT * FROM staff ";
        if (count($params)) {
            foreach ($params as $param => $item) {
                switch ($param) {
                    case 'id':
                        $sql .= " WHERE id = ?";
                        break;
                    default:
                        break;
                }
            }
        }

        if (count($orders)) {
            $order = 'id';
            $direction = 'asc';
            $limit = 20;
            $offset = 0;

            foreach ($orders as $param => $item) {
                switch ($param) {
                    case 'limit':
                        $limit = (int)$item;
                        $limit = $limit ? $limit : 20;
                        break;
                    case 'offset':
                        $offset = (int)$item;
                        $offset = $offset ? $offset : 0;
                        break;
                    case 'order':
                        switch (strtolower($item)) {
                            case 'fname':
                            case 'lname':
                            case 'position_id':
                            case 'revenue':
                            case 'description':
                            case 'id':
                                $order = $item;
                                break;
                            default:
                                $order = 'id';
                                break;
                        }
                        break;
                    case 'direction':
                        switch (strtolower($item)) {
                            case 'asc':
                            case 'desc':
                                $direction = $item;
                                break;
                            default:
                                $direction = 'asc';
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            $sql .= ' ORDER BY ' . $order . ' ' . $direction . ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }
        $data = $this->db->select($sql, $params);

        return $data;
    }

    public function save($params) {
        if (isset($params['id'])) {
            $sql = "UPDATE staff ";
            $sql .= "SET position_id = ?, fname = ?, lname = ?, revenue = ?, description = ? ";
            $sql .= "WHERE id = ?";
            $affected = $this->db->update($sql, $params);

            return $affected;
        } else {
            $sql = "INSERT INTO staff ";
            $sql .= "SET position_id = ?, fname = ?, lname = ?, revenue = ?, description = ? ";
            $id = $this->db->insert($sql, $params);

            return $id;
        }
    }

    public function delete($params) {
        $sql = "DELETE FROM staff WHERE id = ?";
        $affected = $this->db->update($sql, $params);
        return $affected;
    }
}