<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 20:07
 */

namespace App\Services;


use App\Application;

class PositionService
{
    protected $app;
    protected $db;
    protected $schema = '
        CREATE TABLE position (
                     id INT NOT NULL AUTO_INCREMENT PRIMARY KEY, 
                     title CHAR (255) DEFAULT NULL, 
                     description TINYTEXT
        ) ENGINE=InnoDB;
    ';

    public function instantiate()
    {
        $drop = 'DROP TABLE IF EXISTS position';
        $this->db->alter($drop);
        $this->db->alter($this->schema);
    }

    public function __construct(Application $app)
    {
        $this->app = $app;
        $this->db = $app->db();
    }

    public function get($params, $orders)
    {
        $sql = "SELECT * FROM position ";

        if (count($params)) {
            foreach ($params as $param => $item) {
                switch ($param) {
                    case 'id':
                        $params[$param]['value'] = $this->constrain($param, $item['value']);
                        $sql .= " WHERE id = ?";
                        break;
                    default:
                        break;
                }
            }
        }

        if (count($orders)) {
            $order = 'id';
            $direction = 'asc';
            $limit = 20;
            $offset = 0;
            
            foreach ($orders as $param => $item) {
                switch ($param) {
                    case 'limit':
                        $limit = (int)$item;
                        $limit = $limit ? $limit : 20;
                        break;
                    case 'offset':
                        $offset = (int)$item;
                        $offset = $offset ? $offset : 0;
                        break;
                    case 'order':
                        switch (strtolower($item)) {
                            case 'title':
                            case 'description':
                            case 'id':
                                $order = $item;
                                break;
                            default:
                                $order = 'id';
                                break;
                        }
                        break;
                    case 'direction':
                        switch (strtolower($item)) {
                            case 'asc':
                            case 'desc':
                                $direction = $item;
                                break;
                            default:
                                $direction = 'asc';
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }
            $sql .= ' ORDER BY ' . $order . ' ' . $direction . ' LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        $data = $this->db->select($sql, $params);

        return $data;
    }

    public function save($params)
    {
        $params['title']['value'] = $this->constrain('title', $params['title']['value']);
        $params['description']['value'] = $this->constrain('description', $params['description']['value']);

        if (isset($params['id'])) {
            $sql = "UPDATE position ";
            $sql .= "SET title = ?, description = ? ";
            $sql .= "WHERE id = ?";
            $params['id']['value'] = $this->constrain('id', $params['id']['value']);
            $affected = $this->db->update($sql, $params);

            return $affected;
        } else {
            $sql = "INSERT INTO position ";
            $sql .= "SET title = ?, description = ?";
            $id = $this->db->insert($sql, $params);

            return $id;
        }
    }

    public function delete($params)
    {
        $params['id']['value'] = $this->constrain('id', $params['id']['value']);
        $sql = "DELETE FROM position WHERE id = ?";
        $affected = $this->db->update($sql, $params);
        return $affected;
    }

    protected function constrain($field, $value)
    {
        switch ($field) {
            case 'id':
                $value = (int)$value;
                if (!$value) {
                    throw new \Exception('ID must be integer', 400);
                }
                if ($value <= 0) {
                    throw new \Exception('ID must have positive value', 400);
                }
                return $value;
            case 'title':
                $value = strip_tags($value);
                //@TODO May implement some patterns for XSS, SQL-injection catching
                $value = substr($value, 0, 255);
                return $value;
            case 'description':
                $value = strip_tags($value);
                //@TODO May implement some patterns for XSS, SQL-injection catching
                return $value;
            default:
                throw new \Exception('Unknown field', 500);
        }
    }
}