<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 17.09.16
 * Time: 20:45
 */

namespace App\Core;


class Request
{
    private $request;

    public function __construct()
    {
        $request = json_decode(file_get_contents('php://input'), true);
        if (!$request) {
            $request = [];
        }
        $request = array_merge($_GET, $request);

        $this->request = $request;
    }

    public function get($name) {
        return isset($this->request[$name]) ? $this->request[$name] : null;
    }

    public function add($name, $value) {
        $this->request[$name] = $value;
    }

    public function all() {
        return $this->request;
    }
}