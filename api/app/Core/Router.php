<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 22:33
 */

namespace App\Core;

class Router
{
    /**
     * @var Request
     */
    private $request;
    /**
     * @var array
     */
    private $resources;

    /**
     * @var array
     */
    private $routes;

    public $controller;

    public $action;

    /**
     * Router constructor.
     * @param Request $request
     * @param array $resources
     * @param array $routes
     */
    public function __construct(Request $request, array $resources, array $routes = [])
    {
        $this->request = $request;
        $this->resources = $resources;
        $this->routes = $routes;
    }

    /**
     * @return bool
     * @TODO Do implementation for individual routes
     */
    public function matchRoute($method, $path)
    {
        return false;
    }

    public function matchResource($method, $path)
    {
        $resources = array_keys($this->resources);
        $resource = $path[0];
        $id = isset($path[1]) ? $path[1] : false;
        if ($id) {
            $this->request->add('id', $id);
        }
        if (isset($path[2])) {
            return false;
        }

        if (in_array($resource, $resources)) {
            $this->controller = $this->resources[$resource];
            switch ($method) {
                case 'POST':
                    if ($id) {
                        throw new \Exception('Unsupported method for this route', 400);
                    }
                    $this->action = 'add';
                    break;
                case 'GET':
                    $this->action = $id ? 'view' : 'listing';
                    break;
                case 'PUT':
                    if (!$id) {
                        throw new \Exception('Unsupported method for this route', 400);
                    }
                    $this->action = 'update';
                    break;
                case 'DELETE':
                    if (!$id) {
                        throw new \Exception('Unsupported method for this route', 400);
                    }
                    $this->action = 'remove';
                    break;
                case 'OPTIONS':
                    $this->action = 'options';
                    break;
                default:
                    throw new \Exception('Unsupported method', 400);
            }
        } else {
            throw new \Exception('Route not found', 404);
        }
    }

    public function dispatch($method, $path)
    {
        $path = explode('/', $path);

        $controller = $this->matchRoute($method, $path);
        if (!$controller) {
            $controller = $this->matchResource($method, $path);
        }
        return $controller;
    }
}