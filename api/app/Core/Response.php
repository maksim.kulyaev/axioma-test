<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 16.09.16
 * Time: 17:36
 */

namespace App\Core;


class Response
{
    private $httpCode;
    private $headers;
    private $content;

    public function __construct($code, $content)
    {
        $this->httpCode = $code;
        $this->content = $content;
        $this->headers = [
            'Content-Type' => 'application/json',
            'Access-Control-Allow-Methods' => 'POST, GET, PUT, DELETE, OPTIONS',
            'Access-Control-Allow-Origin' => '*',
            'Access-Control-Allow-Headers' => 'accept, content-type'

        ];
    }

    public function json() {
        switch ($this->httpCode) {
            case 200:
                header('HTTP/1.0 200 OK');
                break;
            case 201:
                header('HTTP/1.0 201 Created');
                break;
            case 204:
                header('HTTP/1.0 204 No Content');
                break;
            case 400:
                header('HTTP/1.0 400 Bad Request');
                break;
            case 404:
                header('HTTP/1.0 404 Not Found');
                break;
            case 500:
            default:
                header('HTTP/1.0 500 Internal Server Error');
                break;
        }
        foreach ($this->headers as $key => $value) {
            header("$key: $value");
        }

        echo json_encode($this->content);
    }

}