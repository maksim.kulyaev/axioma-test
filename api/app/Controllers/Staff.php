<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 15.09.16
 * Time: 20:06
 */

namespace App\Controllers;


use App\Application;
use App\Core\Request;
use App\Core\Response;
use App\Services\StaffService;

class Staff
{
    private $service;

    public function __construct(Application $app)
    {
        $this->service = new StaffService($app);
    }

    public function listing(Request $request)
    {
        $order = $request->get('order');
        $direction = $request->get('direction');
        $limit = $request->get('limit');
        $offset = $request->get('offset');

        $params = [

        ];
        $orders = [
            'order' => $order,
            'direction' => $direction,
            'limit' => $limit,
            'offset' => $offset,
        ];
        $data = $this->service->get($params, $orders);
        return new Response(200, $data);
    }

    public function view(Request $request)
    {
        $id = $request->get('id');
        if (!$id) {
            throw new \Exception('ID must be define', 400);
        }

        $params = [
            'id' => ['type' => 'i', 'value' => $id]
        ];

        $data = $this->service->get($params, []);
        if (count($data)) {
            return new Response(200, $data[0]);
        } else {
            throw new \Exception('Position with such ID not found', 404);
        }
    }

    public function add(Request $request)
    {
        //$this->service->instantiate();
        $fname = $request->get('fname');
        $lname = $request->get('lname');
        $position = $request->get('position_id');
        $revenue = $request->get('revenue');
        $description = $request->get('description');

        if (!$fname) {
            throw new \Exception('First name could not be empty!', 400);
        }

        if (!$lname) {
            throw new \Exception('Last name could not be empty!', 400);
        }

        $params = [
            'position_id' => ['type' => 'i', 'value' => $position],
            'fname' => ['type' => 's', 'value' => $fname],
            'lname' => ['type' => 's', 'value' => $lname],
            'revenue' => ['type' => 'f', 'value' => $revenue],
            'description' => ['type' => 's', 'value' => $description],
        ];

        $id = $this->service->save($params);
        $data = ['id' => $id];
        return new Response(201, $data);
    }

    public function update(Request $request)
    {
        $id = $request->get('id');
        $fname = $request->get('fname');
        $lname = $request->get('lname');
        $position = $request->get('position_id');
        $revenue = $request->get('revenue');
        $description = $request->get('description');

        if (!$id) {
            throw new \Exception('ID must be define', 400);
        }

        if (!$fname) {
            throw new \Exception('First name could not be empty!', 400);
        }

        if (!$lname) {
            throw new \Exception('Last name could not be empty!', 400);
        }

        $params = [
            'position_id' => ['type' => 'i', 'value' => $position],
            'fname' => ['type' => 's', 'value' => $fname],
            'lname' => ['type' => 's', 'value' => $lname],
            'revenue' => ['type' => 'f', 'value' => $revenue],
            'description' => ['type' => 's', 'value' => $description],
            'id' => ['type' => 'i', 'value' => $id],
        ];
        $affected = $this->service->save($params);
        $data = ['affected' => $affected];

        return new Response(200, $data);
    }

    public function remove(Request $request)
    {
        $id = $request->get('id');
        if (!$id) {
            throw new \Exception('ID must be define', 400);
        }

        $params = [
            'id' => ['type' => 'i', 'value' => $id]
        ];
        $this->service->delete($params);
        return new Response(204, []);
    }

    public function options(Request $request) {
        return new Response(204, []);
    }
}