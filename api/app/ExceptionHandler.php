<?php
/**
 * Created by PhpStorm.
 * User: maksim
 * Date: 17.09.16
 * Time: 20:03
 */

namespace App;


use App\Core\Response;

class ExceptionHandler
{
    private $exception;

    public function __construct(\Exception $e)
    {
        $this->exception = $e;
    }

    public function handle() {
        $code = $this->exception->getCode();
        $content = [
            'code' => $this->exception->getCode(),
            'message' => $this->exception->getMessage(),
        ];
        $response = new Response($code, $content);
        $response->json();
    }
}